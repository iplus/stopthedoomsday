-module(stopthedoomsday_00_cache).

-export([init/0, stop/1]).

% This script is first executed at server startup and should
% return a list of WatchIDs that should be cancelled in the stop
% function below (stop is executed if the script is ever reloaded).
init() ->
	application:start(inets),
	{data,{mysql_result,_,[[HitsCount]],_,_,_}} = boss_db:execute("select sum(hits) as hits from humen"),
	Info = info:new(1,0),
	Info1 = Info:set(val, HitsCount + 113200),
	Info1:save(),
    {ok, []}.

stop(ListOfWatchIDs) ->
    ok.

