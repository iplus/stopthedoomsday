-module(stopthedoomsday_main_controller, [Req, SessionID]).
-compile([export_all]).

index('GET', [])->
	{{Year,Month,Day},_} = calendar:now_to_datetime( erlang:now() ),
	DaysLeft = 21 - Day,
	DaysText = plural(DaysLeft, "d1", "d2", "d3"),
	LeftText = plural(DaysLeft, "l1", "l2", "l3"),
	HumanCount = boss_db:count(human) + 15210,
    Info = boss_db:find("info-1"),
    HitsCount = Info:val(),
	HitsText = plural(HitsCount, "hits1", "hits2", "hits3"),
	HumansText = plural(HumanCount, "h1", "h2", "h3"),

    SessionUser = boss_session:get_session_data(SessionID, userid),
    if  
        (SessionUser == undefined) -> % аноним
            User = undefined,
            YourHits = undefined; 
        true -> % авторизован
            UserRec = boss_db:find( SessionUser ),
            if
                (UserRec == undefined) -> % не найден юзер
                    User = undefined,
                    YourHits = undefined,
                    boss_session:remove_session_data(SessionID, userid); 
                true ->
                    {human,_,User,_,YourHits} = UserRec
            end
    end,

	{ok, [  
            {user, User}, 
            {hitsText, HitsText}, 
            {humansText, HumansText},  
            {leftText, LeftText},  
            {daysText, DaysText}, 
            {daysLeft, DaysLeft}, 
            { yourHits, YourHits},  
            { hitsCount, HitsCount}, 
            { humanCount, HumanCount}], 
        [{"Content-Language", boss_session:get_session_data(SessionID, locale)}]
    };

index('POST', []) ->
    Token = Req:post_param("token"),
    if 
        ( Token == undefined ) -> error;
        true -> 
            WidgetId = "39081",
            ApiSignature = md5_hex( Token ++ "039f00c636aa0d592b95a6dd10751633" ),
            {ok, {_,_,Info} } = httpc:request("http://loginza.ru/api/authinfo?token=" ++ Token ++ "&id=" ++ WidgetId ++ "&sig=" ++ ApiSignature),
            Res = case mochijson:decode(Info) of 
                {struct, [ {"error_type", ErrType} | _ ] } -> ok;
                {struct, [ {"identity", Identity}, {"provider", Provider} | _ ]} ->  
                    % логинза дала добро
                    User = boss_db:find_first(human, [ { identity, 'equals', Identity } ]),
                    case User of 
                        {human,UserId,_,_,_} -> boss_session:set_session_data(SessionID, userid, UserId);
                        _Else ->
                            % создаем нового юзера
                            NewUser = human:new(id,Identity,Provider,1),
                            {ok,{human,UserId,_,_,_}} = NewUser:save(),
                            boss_session:set_session_data(SessionID, userid, UserId)
                    end;
                _Else -> ok
            end
    end,
    index('GET', []).


en('GET', []) ->
    boss_session:set_session_data(SessionID, locale, "en"),
    {redirect, [{action, "index"}]}.

ru('GET', []) ->
    boss_session:set_session_data(SessionID, locale, "ru"),
    {redirect, [{action, "index"}]}.

updates('GET', [T]) ->
     {ok, Timestamp, Messages} = boss_mq:pull("updates", list_to_integer(T), 300),
     {json, [{timestamp, Timestamp}, {messages, [ {dummy, 0}  ] ++ Messages  }]}.

hit('POST', []) ->
    CountXOR = list_to_integer(Req:post_param("P3")),
    XORBase1 = list_to_integer(Req:post_param("P2")),
    XORBase2 = list_to_integer(Req:post_param("P1")),
    XORBase = (XORBase2 bxor XORBase1),
    Count = (CountXOR bxor XORBase),
    Secret = list_to_integer(Req:post_param("P4")),
    SXor = list_to_integer(Req:post_param("P5")),
    SC2 = list_to_integer(Req:post_param("P6")),
    Secret2 = (SC2 bxor SXor),
    SecsX = list_to_integer(Req:post_param("P7")),
    SecsXOR = list_to_integer(Req:post_param("P8")),
    Secs1 = SecsX bxor SecsXOR,
    Secs2 = min(Secs1, 300000),
    Secs = max(1000, Secs2),
    MaxCount = 7 * Secs / 1000,
    Count1 = min( erlang:abs( Count ), MaxCount), 
    ClicksBeforeX = list_to_integer(Req:post_param("P9")),
    ClicksXor = list_to_integer(Req:post_param("P10")),
    ClicksBefore = ClicksBeforeX bxor ClicksXor,
    if
        (Secret2 /= Secret) -> {output, "ok"};
        true ->
            SessionUser = boss_session:get_session_data(SessionID, userid),
            if  
                 (SessionUser == undefined) -> % аноним
                     {output, "ok"};
                 true -> % авторизован
                     UserRec = boss_db:find( SessionUser ),
                     if
                         (UserRec == undefined) -> % не найден юзер
                             boss_session:remove_session_data(SessionID, userid),
                             {output, "ok"}; 
                         true ->
                             {human,_,User,_,YourHits} = UserRec,
                             if
                                (YourHits =< ClicksBefore) ->
                                    Human1 = UserRec:set(hits, UserRec:hits() + Count1 ),
                                    Human1:save(),
                                    {output, "ok"};
                                true -> 
                                    {output, "ok" }
                             end
                     end
            end
    end.


%% INTERNAL

plural(Value, Form1, Form2, Form5) ->
     V = abs( Value ) rem 100,
     V1 = Value rem 10,
     if (V > 10) and (V < 20) -> Form5;
     	(V1 > 1) and (V < 20) -> Form2;
     	V1 == 1 -> Form1;
     	true -> Form5
     end.


md5_hex(S) ->
    Md5_bin =  erlang:md5(S),
    Md5_list = binary_to_list(Md5_bin),
    lists:flatten(list_to_hex(Md5_list)).
 
list_to_hex(L) ->
    lists:map(fun(X) -> int_to_hex(X) end, L).
 
int_to_hex(N) when N < 256 ->
    [hex(N div 16), hex(N rem 16)].
 
hex(N) when N < 10 ->
    $0+N;
hex(N) when N >= 10, N < 16 ->
        $a + (N-10).
